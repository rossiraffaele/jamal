/*
  Copyright 2013 Rossi Raffaele <rossi.raffaele@gmail.com>

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/
package it.hysteresis.jamal;

import it.hysteresis.jamal.Select.Option;
import it.hysteresis.jamal.Select.OptGroup;

abstract public class SelectElementBuilder<T extends FormElementBuilder>
extends FormElementBuilder<T> {

  SelectElementBuilder(Form parent, String tagName) {
    super(parent,tagName);
  }

  public Option option(String value,String label) {
    Option option = new Option(this, value, label);
    _element.appendChild(option._element);
    return option;
  }

  public Option option(String value, Enum label) {
    return option(value, _dictionary.getLabel(label));
  }

  abstract public OptGroup optgroup(String label);
  public OptGroup optgroup(Enum label) {
    return optgroup(_dictionary.getLabel(label));
  }
}
