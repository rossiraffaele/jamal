/*
  Copyright 2013 Rossi Raffaele <rossi.raffaele@gmail.com>

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/
package it.hysteresis.jamal;
import org.w3c.dom.Document;

public class A extends WebPageElement<A> {

  static public enum Target {
    BLANK("_blank"),
    SELF("_self");

    private String _attribute;
    Target(String att) {
      _attribute = att;
    }

    public String getAttribute() { return _attribute; }
  }

  A( WebPageElementBuilder parentBuilder ) {
    super(parentBuilder,"a");
    wrap(this);
  }
  A( WebPageElementBuilder parentBuilder, String name ) {
    this(parentBuilder);
    setAttribute("name",name);
  }

  A( WebPageElementBuilder parentBuilder, String href, String text ) {
    this(parentBuilder);
    setHref(href);
    setTextContent(text);
  }

  A( WebPageElementBuilder parentBuilder, String href, WebPageElement child ) {
    this(parentBuilder);
    setHref(href);
    _element.appendChild(child._element);
  }

  public A setHref(String url) {
    return setAttribute("href", url);
  }

  public A setTarget(Target target) {
    return setAttribute( "target", target.getAttribute() );
  }
}
