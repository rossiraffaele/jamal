/*
  Copyright 2013 Rossi Raffaele <rossi.raffaele@gmail.com>

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/
package it.hysteresis.jamal;

import org.w3c.dom.Element;

public class Table extends WebPageElement<Table> {

  Table(WebPageElementBuilder parent) {
    this(parent,null);
  }

  Table(WebPageElementBuilder parent, String[] headers) {
    super(parent,"table");
    wrap(this);
    if (headers != null) {
      Row r = row();
      for (String h : headers) {
        r.th(h);
      }
    }
  }

  public Row row() {
    Row tr = new Row(this);
    _element.appendChild(tr._element);
    return tr;
  }

  public static class Row extends WebPageElement<Row> {
    private Table _parentTable;

    Row(Table parentTable) {
      super(parentTable._parentBuilder,"tr");
      wrap(this);
      _parentTable = parentTable;
    }

    public Cell th(String header) {
      Cell cell = new Cell(this, "th").setTextContent(header);
      _element.appendChild( cell._element );
      return cell;
    }

    public Cell th(WebPageElement e) {
      Cell cell = new Cell(this, "th");
      cell._element.appendChild(e._element);
      _element.appendChild( cell._element );
      return cell;
    }

    public Cell td(String data) {
      Cell cell = new Cell(this,"td").setTextContent(data);
      _element.appendChild( cell._element );
      return cell;
    }

    public Cell td(WebPageElement e) {
      Cell cell = new Cell(this,"td");
      cell._element.appendChild(e._element);
      _element.appendChild(cell._element);
      return cell;
    }

    public Row row() {
      return _parentTable.row();
    }

    public static class Cell extends WebPageElement<Cell> {
      private Row _parentRow;
      Cell(Row parentRow, String cellTagName) {
        super( parentRow._parentBuilder, cellTagName );
        wrap(this);
        _parentRow = parentRow;
      }

      @Override
      public <T extends WebPageElement> T append(T element) {
        _element.appendChild( element._element );
        return element;
      }

      public Cell th(String header) {
        return _parentRow.th(header);
      }

      public Cell td(String data) {
        return _parentRow.td(data);
      }

      public Cell td(WebPageElement e) {
        return _parentRow.td(e);
      }

      public Row row() {
        return _parentRow.row();
      }

      public Cell setColSpan(int colspan) {
        _element.setAttribute("colspan", Integer.toString(colspan));
        return this;
      }

      public Cell setRowSpan(int rowspan) {
        _element.setAttribute("rowspan", Integer.toString(rowspan));
        return this;
      }
    }
  }

}
