/*
  Copyright 2013 Rossi Raffaele <rossi.raffaele@gmail.com>

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/
package it.hysteresis.jamal;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

public class Input extends FormElementBuilder<Input> {

  static public enum Type {
    CHECKBOX("checkbox"),
    DATE("date"),
    EMAIL("email"),
    HIDDEN("hidden"),
    NUMBER("number"),
    PASSWORD("password"),
    SUBMIT("submit"),
    TEXT("text"),
    TIME("time");
    private String _type;
    Type(String type) {
      _type = type;
    }
    public String getType() {
      return _type;
    }
  }

  Input(Form parent, Type type, String name) {
    super(parent,"input");
    wrap(this);
    _element.setAttribute("type",type.getType());
    if (name != null) {
      _element.setAttribute("name",name);
    }
  }

  Input(Form parent, Type type) {
    this(parent,type,null);
  }

  public Input setValue(String value) {
    return setAttribute("value",value);
  }

  public Input setValue(Enum value) {
    return setAttribute("value", _dictionary.getLabel(value)); 
  }

  public Input setChecked() {
    return setAttribute("checked", "checked");
  }

  public Input setMax(int max) {
    return setAttribute("max", Integer.toString(max));
  }

  public Input setMin(int min) {
    return setAttribute("min", Integer.toString(min));
  }

  public Input setStep(double step) {
    return setAttribute("step", Double.toString(step));
  }

  public Input setChecked(boolean checked) {
    if (checked) {
      return setAttribute("checked", "checked");
    } else {
      return this;
    }
  }

  public Input setPlaceHolder(String placeholder) {
    return setAttribute("placeholder", placeholder);
  }

  public Input setPlaceHolder(Enum placeholder) {
    return setAttribute("placeholder", _dictionary.getLabel(placeholder));
  }
}
