/*
  Copyright 2013 Rossi Raffaele <rossi.raffaele@gmail.com>

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/
package it.hysteresis.jamal;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import it.hysteresis.jamal.i18n.Dictionary;

class WebPageElementBuilder {

  static public final String CLASS_BUTTON = "jamal-button";
  static public final String CLASS_BUTTON_ICON = "jamal-button-icon";
  static public final String CLASS_BUTTON_TEXT = "jamal-button-text";

  protected Element _containerElement;
  protected Document _document;
  protected WebPageElementBuilder _parentBuilder;
  protected Dictionary _dictionary;

  protected void initRootBuilder(Document document, Element container) {
    _document = document;
    _containerElement = container;
    _parentBuilder = this;
  }

  WebPageElementBuilder() {
    _parentBuilder = this;
    _document = null;
    _containerElement = null;
    _dictionary = null;
  }

  WebPageElementBuilder(WebPageElementBuilder parent) {
    _parentBuilder = parent;
    _dictionary = _parentBuilder._dictionary;
    initRootBuilder(parent._document, parent._containerElement);
  }

  protected Element createElement( String tagName ) {
    return _document.createElement(tagName);
  }

  public <T extends WebPageElement> T append(T element) {
    _containerElement.appendChild( element._element );
    return element;
  }

  public A a(String name) {
    return append( new A(_parentBuilder, name) );
  }

  public A a(String href, String text) {
    return append( new A(_parentBuilder,href,text) );
  }

  public A a(String href, Enum text) {
    return a(href, _dictionary.getLabel(text));
  }

  public A a(String href, WebPageElement child) {
    return append( new A(_parentBuilder,href,child) );
  }

  public A button(String href, String text) {
    return button(href, text, "");
  }

  public A button(String href, String text, String icon) {
    Div button = div().setClassName(CLASS_BUTTON);
    button.p(icon).setClassName(CLASS_BUTTON_ICON);
    button.p(text).setClassName(CLASS_BUTTON_TEXT);
    return a(href, button).setTitle(text);
  }

  public A button(String href, Enum text) {
    return button(href,_dictionary.getLabel(text));
  }

  public A button(String href, Enum text, Enum icon) {
    return button(href, _dictionary.getLabel(text), _dictionary.getLabel(icon));
  }


  public Details details(String summary) {
    return append(new Details(_parentBuilder,summary));
  }

  public Div div() {
    return append( new Div(_parentBuilder) );
  }

  public FlowLayout flowLayout() {
    return append(new FlowLayout(_parentBuilder));
  }

  public Form form() {
    return append( new Form(_parentBuilder).setMethod(Form.Method.POST) );
  }

  public Form form(Form.Method method) {
    return append( new Form(_parentBuilder).setMethod(method) );
  }

  public Img img(String url) {
    return append( new Img(_parentBuilder,url) );
  }

  public KeyValueWidget keyValueWidget() {
    return append(new KeyValueWidget(_parentBuilder) );
  }

  public P p( String text ) {
    return append( new P(_parentBuilder,text) );
  }

  public P p(Enum text) {
    return p(_dictionary.getLabel(text));
  }

  public Pre pre( String text ) {
    return append( new Pre(_parentBuilder,text) );
  }

  public Table table() {
    return append( new Table(_parentBuilder) );
  }

  public Table table(String[] headers) {
    return append( new Table(_parentBuilder, headers) );
  }

  public Ul ul() {
    return append( new Ul(_parentBuilder) );
  }
};
