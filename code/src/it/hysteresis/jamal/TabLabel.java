/*
  Copyright 2013 Rossi Raffaele <rossi.raffaele@gmail.com>

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/
package it.hysteresis.jamal;

public class TabLabel {

  static private final String CLASS_TAB = "jamal-tabWidget-tab";
  static private final String CLASS_SELECTED = "selected";

  private Div _wrapper;

  TabLabel( Div tabsContainer, WebPageElement tab ) {
    _wrapper = tabsContainer.div().setClassName(CLASS_TAB);
    _wrapper.append(tab);
  }

  public void setSelected(boolean selected) {
    if (selected) {
      _wrapper.addClassName(CLASS_SELECTED);
    }
  }

}
