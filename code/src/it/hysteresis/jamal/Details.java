/*
  Copyright 2013 Rossi Raffaele <rossi.raffaele@gmail.com>

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/
package it.hysteresis.jamal;

public class Details extends WebPageElement<Details> {
  Details( WebPageElementBuilder parent, String summary){
    super(parent, "details");
    wrap(this);
    initRootBuilder( parent._document, _element );
    append(new Summary(this, summary));
  }

  public class Summary extends WebPageElement<Summary> {
    Summary(WebPageElementBuilder parent, String summary) {
      super(parent,"summary");
      setTextContent(summary);
    }
  }

  public Details open() {
    return setAttribute("open","open");
  }
}
