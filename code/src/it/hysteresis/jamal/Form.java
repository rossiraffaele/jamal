/*
  Copyright 2013 Rossi Raffaele <rossi.raffaele@gmail.com>

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/
package it.hysteresis.jamal;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

import it.hysteresis.jamal.Input.Type;
import it.hysteresis.jamal.Label;

public class Form extends FormElementBuilder<Form> {

  static public enum Method {
    GET("get"),
    POST("post");

    private String _method;
    Method(String method) {
      _method = method;
    }

    public String getMethod() {
      return _method;
    }
  }

  Form(WebPageElementBuilder parent) {
    super(parent, "form");
    wrap(this);
  }

  public Form setMethod( Method method ) {
    _element.setAttribute("method", method.getMethod() );
    return this;
  }

  public Form setAction( String action ) {
    _element.setAttribute("action", action);
    return this;
  }

}
