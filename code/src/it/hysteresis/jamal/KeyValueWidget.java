/*
  Copyright 2013 Rossi Raffaele <rossi.raffaele@gmail.com>

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/
package it.hysteresis.jamal;

public class KeyValueWidget extends WebPageElement<KeyValueWidget> {

  static public final String CLASS_WIDGET = "jamal-key-value-widget";
  static public final String CLASS_PAIR = "jamal-key-value-pair";
  static public final String CLASS_KEY = "jamal-key";
  static public final String CLASS_VALUE = "jamal-value";
  static public final String CLASS_SECTION = "jamal-key-value-pair-section";
  static public final String CLASS_CONTROLS = "jamal-key-value-pair-controls";

  KeyValueWidget(WebPageElementBuilder parent) {
    super(parent, "div");
    wrap(this);
    initRootBuilder( parent._document, _element );
    setClassName(CLASS_WIDGET);
  }

  public KeyValueWidget addSectionBreak(Enum label) {
    div().setClassName(CLASS_PAIR)
      .p(label).setClassName(CLASS_SECTION)
      .p("").setClassName(CLASS_VALUE);
    return this;
  }

  public KeyValueWidget addKeyValue(Enum key, Enum value) {
    return addKeyValue(_dictionary.getLabel(key), _dictionary.getLabel(value));
  }

  public KeyValueWidget addKeyValue(Enum key, String value) {
    return addKeyValue(_dictionary.getLabel(key), value);
  }

  public KeyValueWidget addKeyValue(String key, String value) {
    div().setClassName(CLASS_PAIR)
      .p(key).setClassName(CLASS_KEY)
      .p(value).setClassName(CLASS_VALUE);
    return this;
  }

  public KeyValueWidget addKeyValue(String key, String value, Div controls) {
    div().setClassName(CLASS_PAIR)
      .p(key).setClassName(CLASS_KEY)
      .p(value).setClassName(CLASS_VALUE)
      .append(controls).addClassName(CLASS_CONTROLS);
    return this;
  }

  public KeyValueWidget addKeyValue(String key, WebPageElement value) {
    div().setClassName(CLASS_PAIR)
      .p(key).setClassName(CLASS_KEY)
      .append(value).setClassName(CLASS_VALUE);
    return this;
  }

  public KeyValueWidget addKeyValue(String key, WebPageElement value, Div controls) {
    Div row = div().setClassName(CLASS_PAIR);
    row.p(key).setClassName(CLASS_KEY)
      .append(value).setClassName(CLASS_VALUE);
    row.append(controls).addClassName(CLASS_CONTROLS);
    return this;
  }

  public KeyValueWidget addKeyValue(Enum key, WebPageElement value, Div controls) {
    return addKeyValue(_dictionary.getLabel(key), value, controls);
  }

}
