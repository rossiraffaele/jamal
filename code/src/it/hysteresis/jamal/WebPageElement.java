/*
  Copyright 2013 Rossi Raffaele <rossi.raffaele@gmail.com>

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/
package it.hysteresis.jamal;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

public class WebPageElement<T extends WebPageElement>
extends WebPageElementBuilder {
  protected Element _element;
  protected T _this;
  protected WebPageElement(WebPageElementBuilder parent, String tagName) {
    super(parent);
    _parentBuilder = parent;
    _element = createElement(tagName);
  }

  protected void wrap(T t) {
    _this = t;
  }

  public String getAttribute(String name) {
    return _element.getAttribute(name);
  }

  public T setAttribute(String name, String value) {
    _element.setAttribute(name, value);
    return _this;
  }

  public T setTextContent(String text) {
    _element.setTextContent(text);
    return _this;
  }

  public T setTitle(String text) {
    return setAttribute("title", text);
  }

  public String getId() {
    return getAttribute("id");
  }

  public T setId(String id) {
    return setAttribute("id",id);
  }

  public T setClassName(String className) {
    return setAttribute("class", className);
  }

  public T addClassName(String className) {
    return setAttribute("class", getAttribute("class")+" "+className);
  }

}
