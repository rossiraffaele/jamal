/*
  Copyright 2013 Rossi Raffaele <rossi.raffaele@gmail.com>

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/
package it.hysteresis.jamal;
import org.w3c.dom.Document;

public class Select extends SelectElementBuilder<Select> {

  Select(Form parent, String name) {
    super(parent,"select");
    wrap(this);
    setAttribute("name",name);
  }

  public Option option() {
    return option(null, (String)null);
  }

  @Override
  public OptGroup optgroup(String label) {
    OptGroup optgroup = new OptGroup(this, label);
    _element.appendChild(optgroup._element);
    return optgroup;
  }

  static public class OptGroup extends SelectElementBuilder<OptGroup> {
    Select _parentSelect;
    OptGroup(Select parentSelect, String label) {
      super(parentSelect._parentForm,"optgroup");
      wrap(this);
      _parentSelect = parentSelect;
      setAttribute("label", label);
    }

    @Override
    public OptGroup optgroup(String label) {
      return _parentSelect.optgroup(label);
    }
  }

  static public class Option extends SelectElementBuilder<Option> {
    SelectElementBuilder _parentBuilder;

    Option(SelectElementBuilder parentBuilder, String value, String label) {
      super(parentBuilder._parentForm,"option");
      wrap(this);
      _parentBuilder = parentBuilder;
      if (value != null) {
        setAttribute("value",value);
      }
      if (label != null) {
        setTextContent(label);
      }
    }

    public Option setSelected() {
      return setAttribute("selected","selected");
    }

    public Option option() {
      return _parentBuilder.option(null, (String)null);
    }

    @Override
    public Option option(String value,String label) {
      return _parentBuilder.option(value,label);
    }

    @Override
    public OptGroup optgroup(String label) {
      return _parentBuilder.optgroup(label);
    }
  }
}
