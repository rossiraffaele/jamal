/*
  Copyright 2013 Rossi Raffaele <rossi.raffaele@gmail.com>

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/
package it.hysteresis.jamal;

public class TabWidget {

  static private final String CLASS_WIDGET = "jamal-tabWidget";
  static private final String CLASS_TABSLIST = "jamal-tabWidget-tabslist";
  static private final String CLASS_TAB = "jamal-tabWidget-tab";
  static private final String CLASS_SELECTED = "selected";
  static private final String CLASS_CONTENT = "jamal-tabWidget-content";

  private Div _wrapper;
  private Div _tabsContainer;
  private Div _contentContainer;

  public TabWidget( WebPageElementBuilder parent ) {
    _wrapper = parent.div().setClassName(CLASS_WIDGET);
    _tabsContainer = _wrapper.div().setClassName(CLASS_TABSLIST);
    _contentContainer = _wrapper.div().setClassName(CLASS_CONTENT);
  }

  public Div getContentContainer() {
    return _contentContainer;
  }

  public <T extends WebPageElement> T setContent(T element) {
    return _contentContainer.append(element);
  }

  public TabLabel addTab( WebPageElement tab ) {
    return new TabLabel(_tabsContainer, tab);
  }

}
