/*
  Copyright 2013 Rossi Raffaele <rossi.raffaele@gmail.com>

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/
package it.hysteresis.jamal;

import org.w3c.dom.Document;

import it.hysteresis.jamal.Input.Type;

class FormElementBuilder<T extends WebPageElement> extends WebPageElement<T> {

  protected Form _parentForm;
  protected FormElementBuilder(Form parent, String tagName) {
    super(parent._parentBuilder, tagName);
    _parentForm = parent;
    initRootBuilder(_document, _element);
  }

  protected FormElementBuilder(WebPageElementBuilder parent, String tagName) {
    super(parent, tagName);
    _parentForm = null;
    initRootBuilder(_document, _element);
  }

  @Override
  protected void wrap(T t) {
    super.wrap(t);
    if (_parentForm == null) {
      _parentForm = (Form)this._this;
    }
  }

  private <T extends WebPageElement> T appendFormElement(T formElement) {
    _parentForm._element.appendChild(formElement._element);
    return formElement;
  }

  public Label label(String forElement, Enum text) {
    String s = _dictionary.getLabel(text);
    return appendFormElement( new Label(_parentForm, forElement, s) );
  }

  public Label label(String forElement, String text) {
    return appendFormElement( new Label(_parentForm, forElement, text) );
  }

  public Input input(String name, Input.Type type) {
    return appendFormElement( new Input(_parentForm, type, name) );
  }

  public Input submit() {
    return appendFormElement( new Input(_parentForm, Input.Type.SUBMIT) );
  }

  public Select select(String name) {
    return appendFormElement( new Select(_parentForm,name) );
  }

  public TextArea textarea(String name) {
    return appendFormElement( new TextArea(_parentForm, name) );
  }
}
