/*
  Copyright 2013 Rossi Raffaele <rossi.raffaele@gmail.com>

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/
package it.hysteresis.jamal;

import java.io.StringWriter;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

import it.hysteresis.jamal.Form.Method;
import it.hysteresis.jamal.i18n.Dictionary;

public class WebPage extends WebPageElementBuilder {
  private Document _document;
  private Element _root;
  private Element _head;
  private Element _body;
  private Element _title;

  public WebPage(DocumentBuilder docBuilder) {
    init(docBuilder);
  }

  public WebPage(Dictionary dictionary) {
    _dictionary = dictionary;
    try {
      DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
      DocumentBuilder docBuilder = factory.newDocumentBuilder();
      init(docBuilder);
    } catch (Exception e) {
      throw new RuntimeException("Can't build XHTML document",e);
    }
  }

  public WebPage() {
    this((Dictionary)null);
  }

  private void init(DocumentBuilder docBuilder) {
    _document = docBuilder.newDocument();
    _root = _document.createElementNS("http://www.w3.org/1999/xhtml","html");
    _head = _document.createElement("head");
    _body = _document.createElement("body");
    _title = null;
    _document.appendChild(_root);
    _root.appendChild(_head);
    _root.appendChild(_body);
    initRootBuilder(_document,_body);
  }

  public Element getHead() {
    return _head;
  }

  public Element getBody() {
    return _body;
  }

  public void setTitle(String title) {
    _title = _document.createElement("title");
    _title.setTextContent(title);
  }

  public void addMeta(String name, String content) {
    Element meta = _document.createElement("meta");
    meta.setAttribute("name",name);
    meta.setAttribute("content", content);
    _head.appendChild(meta);
  }

  public void addViewportMeta(String content) {
    addMeta("viewport",content);
  }

  public void addScript(String url) {
    Element script = _document.createElement("script");
    script.setAttribute("src", url);
    _head.appendChild(script);
  }

  public void addLink(LinkType type, String url) {
    Element link = _document.createElement("link");
    link.setAttribute("rel", type.getRel());
    link.setAttribute("type", type.getType());
    link.setAttribute("href",url);
    _head.appendChild(link);
  }

  private void setTitle() {
    if (_title == null) {
      setTitle("");
    }
    _head.appendChild(_title);
  }

  private void prepareDocumentForRendering() {
    setTitle();
  }

  @Override
  public String toString() {
    try {
      prepareDocumentForRendering();
      TransformerFactory factory = TransformerFactory.newInstance();
      Transformer transformer = factory.newTransformer();
      transformer.setOutputProperty(OutputKeys.ENCODING,"utf-16");
      transformer.setOutputProperty(OutputKeys.DOCTYPE_PUBLIC,
        "-//W3C//DTD XHTML 1.0 Transitional//EN");
      transformer.setOutputProperty(OutputKeys.DOCTYPE_SYSTEM,
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd");
      DOMSource source = new DOMSource(_document);
      StringWriter writer = new StringWriter();
      transformer.transform(source, new StreamResult(writer));
      String result = writer.toString();
      return result;
    } catch (Exception e) {
      throw new RuntimeException("Can't render XHTML document", e);
    }
  }

}
